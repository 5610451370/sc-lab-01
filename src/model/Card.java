package model;

public class Card {
	private int balance;
	private String name;
	private int amount;

	public Card(int balance) {
		this.balance = balance;
	}
	
	public void withdraw(int amount){
		 balance-=amount;
	}
	
	public void deposit(int amount){
		 balance+=amount;
	}

	public void setName(String str) {
		name = str;
	}
	
	public int getBalance(){
		return balance;
	}

	public String toString() {
		return name;
	}

}
